package com.powerhouse.util;

/**
 * Created by bilalilyas on 21/03/17.
 */

public enum Month {
    JAN,
    FEB,
    MAR,
    APR,
    MAY,
    JUN,
    JUL,
    AUG,
    SEP,
    OCT,
    NOV,
    DEC
}
