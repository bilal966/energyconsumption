package com.powerhouse.util;

import com.powerhouse.model.MeterReading;
import com.powerhouse.model.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by bilalilyas on 03/04/17.
 */
public class Validation {

    private static final Logger log = LoggerFactory.getLogger(Validation.class);

    private static int totalYearlyReading = 0;

    /**
     * @param profile
     * @return true if profile is valid else false
     */

    public static boolean validateProfile(Profile profile){
        boolean isValidProfile = false;
        if (profile == null || profile.getMapMonthFraction() == null || profile.getMapMonthFraction().size() == 0)
            return false;
        Map<String,Double> monthFractionMap =  profile.getMapMonthFraction();
        Double totalFractions = 0.0;
        for(Double fraction: monthFractionMap.values()){
            totalFractions = totalFractions+fraction;
        }
        if(totalFractions == 1)
            isValidProfile= true;
        return isValidProfile;
    }


    public static boolean validateMeterReading(MeterReading meter){
        boolean isValidMeterReading = false;
        if(validateMonthlyReading(meter.getMonthlyReading())){
            return false;
        }

        return isValidMeterReading;
    }

    /**Assuming the list has months details in sequence from Jan to Dec*/
    public static boolean validateMonthlyReading(Map<String,Integer> monthlyReading){
        boolean isValid = true;
        Map.Entry<String,Integer> prev = null;
        for(Map.Entry<String,Integer> entry : monthlyReading.entrySet()){
            if(entry.getKey() == ApplicationConstant.monthJan){
                prev = entry;
            }
            if(prev != null && entry.getValue() < prev.getValue()){
                isValid = false;
            }
            totalYearlyReading = totalYearlyReading+entry.getValue();
            prev = entry;
        }
        return isValid;
    }

    public static boolean validateMonthlyConsumptionWithFraction(
            Map<String, Integer> monthlyReading,
            Map<String, Double> monthlyFraction) {
        boolean isValid = true;
        if(monthlyFraction.isEmpty()){
            log.info("The fractions are not available for month reading");
            return false;
        }
        return isValid;
    }

}
