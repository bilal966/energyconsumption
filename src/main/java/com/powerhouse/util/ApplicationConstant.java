package com.powerhouse.util;

/**
 * Created by bilalilyas on 21/03/17.
 */
public class ApplicationConstant {

    public static final String monthJan = Month.JAN.toString();
    public static final String monthFeb = Month.FEB.toString();
    public static final String monthMar = Month.MAR.toString();
    public static final String monthApr = Month.APR.toString();
    public static final String monthMay = Month.MAY.toString();
    public static final String monthJun = Month.JUN.toString();
    public static final String monthJul = Month.JUL.toString();
    public static final String monthAug = Month.AUG.toString();
    public static final String monthSep = Month.SEP.toString();
    public static final String monthOct = Month.OCT.toString();
    public static final String monthNov = Month.NOV.toString();
    public static final String monthDec = Month.DEC.toString();
}
