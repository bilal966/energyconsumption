package com.powerhouse.util;

import com.powerhouse.model.MeterReading;
import com.powerhouse.model.Profile;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by bilalilyas on 04/04/17.
 */
public class ApplicationUtil {

    public static Profile prepareProfile(String profileName) {

        Map<String,Double> monthFractionMap =  new HashMap<String,Double>(12);
        monthFractionMap.put(ApplicationConstant.monthJan, 0.15);
        monthFractionMap.put(ApplicationConstant.monthFeb, 0.17);
        monthFractionMap.put(ApplicationConstant.monthMar, 0.13);
        monthFractionMap.put(ApplicationConstant.monthApr, 0.08);
        monthFractionMap.put(ApplicationConstant.monthMay, 0.08);
        monthFractionMap.put(ApplicationConstant.monthJun, 0.00);
        monthFractionMap.put(ApplicationConstant.monthJul, 0.00);
        monthFractionMap.put(ApplicationConstant.monthAug, 0.01);
        monthFractionMap.put(ApplicationConstant.monthSep, 0.04);
        monthFractionMap.put(ApplicationConstant.monthOct, 0.09);
        monthFractionMap.put(ApplicationConstant.monthNov, 0.10);
        //monthFractionMap.put(ApplicationConstant.monthNov, 0.35);
        monthFractionMap.put(ApplicationConstant.monthDec, 0.15);


        Profile profile = new Profile();
        profile.setProfileName(profileName);
        profile.setMapMonthFraction(monthFractionMap);
        return profile;
    }

    public static MeterReading prepareMeterReading(int meterId, String profileName) {
        Map<String,Integer> monthlyReading = new LinkedHashMap<String,Integer>(12);
        monthlyReading.put(ApplicationConstant.monthJan, 36);
        monthlyReading.put(ApplicationConstant.monthFeb, 77);
        monthlyReading.put(ApplicationConstant.monthMar, 108);
        monthlyReading.put(ApplicationConstant.monthApr, 127);
        monthlyReading.put(ApplicationConstant.monthMay, 146);
        monthlyReading.put(ApplicationConstant.monthJun, 146);
        monthlyReading.put(ApplicationConstant.monthJul, 146);
        monthlyReading.put(ApplicationConstant.monthAug, 148);
        monthlyReading.put(ApplicationConstant.monthSep, 158);
        monthlyReading.put(ApplicationConstant.monthOct, 180);
        monthlyReading.put(ApplicationConstant.monthNov, 204);
        monthlyReading.put(ApplicationConstant.monthDec, 240);

        MeterReading meter = new MeterReading();
        meter.setMeterId(meterId);
        meter.setProfileName(profileName);
        meter.setMonthlyReading(monthlyReading);
        return meter;
    }
}
