package com.powerhouse.model;

import java.util.HashMap;
import java.util.Map;

/**
 * This is model fo Profile.
 *
 * Created by bilalilyas on 21/03/17.
 */

public class Profile {

    private String profileName;

    private Map<String, Double> mapMonthFraction = new HashMap<String, Double>(12);

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public Map<String, Double> getMapMonthFraction() {
        return mapMonthFraction;
    }

    public void setMapMonthFraction(Map<String, Double> mapMonthFraction) {
        this.mapMonthFraction = mapMonthFraction;
    }

    @Override
    public String toString() {
        return "Profile [ProfileName=" + profileName + ", MapFractionMonth="
                + mapMonthFraction + "]";
    }
}
