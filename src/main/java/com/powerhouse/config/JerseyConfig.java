package com.powerhouse.config;


import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

/**
 * Created by bilalilyas on 22/03/17.
 */

@Configuration
@Component
@ApplicationPath("/demo")
public class JerseyConfig extends ResourceConfig{

    public JerseyConfig() {
        packages("com.powerhouse.controller");
        //register(MyController.class);
       //register(CountryController.class);
    }

}
