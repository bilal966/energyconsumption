package com.powerhouse.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Created by bilalilyas on 02/04/17.
 */


@Configuration
public class ApplicationConfiguration {

    @Value("${application.title}")
    private String title;

    @Value("${legacy.dir}")
    private String legacyFilesDir;

    @Value("${legacy.file.profiles}")
    private String fileProfiles;

    @Value("${legacy.file.meter.readings}")
    private String fileMeterReading;

    public String getTitle() {
        return title;
    }

    public String getLegacyFilesDir() {
        return legacyFilesDir;
    }

    public String getFileProfiles() {
        return fileProfiles;
    }

    public String getFileMeterReading() {
        return fileMeterReading;
    }
}
