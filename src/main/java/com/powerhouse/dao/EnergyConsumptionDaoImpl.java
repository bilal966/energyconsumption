package com.powerhouse.dao;

import com.powerhouse.businesslogic.MonthlyConsumptionProcessor;
import com.powerhouse.model.MeterReading;
import com.powerhouse.model.Profile;
import com.powerhouse.util.Validation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by bilalilyas on 03/04/17.
 */

@Service
public class EnergyConsumptionDaoImpl implements IEnergyConsumptionDao {

    private static final Logger log = LoggerFactory.getLogger(EnergyConsumptionDaoImpl.class);

    private final String TABLE_METER_READING = "meter_reading";
    private final String TABLE_PROFILE = "profile";

    //@Autowired
    private NamedParameterJdbcTemplate namedParamJdbcTemplate = new NamedParameterJdbcTemplate(DBConnection.getDataSource());

    private Map<String, Map<String, Double>> profileMonthFractionMap = new HashMap<String, Map<String, Double>>();

    private final String GET_METER_READING = "SELECT * FROM " + TABLE_METER_READING + " WHERE meter_id=:meterId";
    private final String INSERT_PROFILE = "INSERT INTO " + TABLE_PROFILE + " (profile_name, month, fraction,created_date,updated_date) values (:profile,:month,:fraction,CURRENT_DATE,CURRENT_DATE)";
    private final String GET_PROFILE_BY_NAME = "SELECT * FROM " + TABLE_PROFILE + " WHERE profile_name = :profile";
    private final String INSERT_METER_READING = "INSERT INTO " + TABLE_METER_READING + " (meter_id,profile_name, month, reading,consumption) values (:meterId,:profile,:month,:reading,:consumption)";

    @Override
    public void insertProfile(Profile profile) {
        if (Validation.validateProfile(profile)) {

            List<Map<String, Object>> batchValues = new ArrayList<>(profile
                    .getMapMonthFraction().size());
            for (Map.Entry<String, Double> entry : profile.getMapMonthFraction()
                    .entrySet()) {
                batchValues.add(new MapSqlParameterSource("profile", profile
                        .getProfileName()).addValue("month", entry.getKey())
                        .addValue("fraction", entry.getValue()).getValues());
            }

            int[] updateCounts = namedParamJdbcTemplate.batchUpdate(
                    INSERT_PROFILE, batchValues.toArray(new Map[profile
                            .getMapMonthFraction().size()]));
            log.info("rows inserted: " + updateCounts.length);
            if (updateCounts.length > 0) {
                profileMonthFractionMap.put(profile.getProfileName(),
                        profile.getMapMonthFraction());
            }
        } else {
            log.debug("the input profile is not valid, since total of all fraction is not 1");
        }

    }

    @Override
    public Profile getProfile(String profileName) {

        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("profile", profileName);
        Map<String, Double> monthFractionMap = namedParamJdbcTemplate.query(
                GET_PROFILE_BY_NAME, paramMap,
                new ResultSetExtractor<Map<String, Double>>() {
                    @Override
                    public Map<String, Double> extractData(ResultSet rs)
                            throws SQLException, DataAccessException {
                        Map<String, Double> map = new HashMap<String, Double>(
                                12);
                        while (rs.next()) {
                            map.put(rs.getString("month"),
                                    rs.getDouble("fraction"));
                        }
                        return map;
                    }
                });
        Profile p = new Profile();
        if (monthFractionMap.size() > 0) {
            p.setProfileName(profileName);
            p.setMapMonthFraction(monthFractionMap);
        }
        return p;
    }

    @Override
    public void insertMeterReading(MeterReading meter) {
        boolean isValidReading = Validation.validateMonthlyReading(meter
                .getMonthlyReading());
        boolean isValidConsumption = Validation
                .validateMonthlyConsumptionWithFraction(meter
                        .getMonthlyReading(), this.profileMonthFractionMap
                        .get(meter.getProfileName()));

        if (isValidReading && isValidConsumption) {
            /** calculating and setting monthly consumption */
            Map<String, Integer> monthlyConsumption = new MonthlyConsumptionProcessor()
                    .processMonthlyConsumption(meter.getMonthlyReading());
            List<Map<String, Object>> batchValues = new ArrayList<>(meter
                    .getMonthlyReading().size());
            for (Map.Entry<String, Integer> entry : meter.getMonthlyReading()
                    .entrySet()) {
                batchValues.add(new MapSqlParameterSource("meterId", meter
                        .getMeterId())
                        .addValue("profile", meter.getProfileName())
                        .addValue("month", entry.getKey())
                        .addValue("consumption",
                                monthlyConsumption.get(entry.getKey()))
                        .addValue("reading", entry.getValue()).getValues());
            }

            int[] updateCounts = namedParamJdbcTemplate.batchUpdate(
                    INSERT_METER_READING, batchValues.toArray(new Map[meter
                            .getMonthlyReading().size()]));
            log.info("rows inserted: " + updateCounts.length);
        } else {
            log.info("The input meter data is not valid");
        }
    }

    @Override
    public MeterReading getMeterReading(int meterId) {
        Map<String, Integer> paramMap = new HashMap<String, Integer>();
        paramMap.put("meterId", meterId);
        MeterReading meter = new MeterReading();
        Map<String, Integer> monthlyReading = new LinkedHashMap<String, Integer>();
        Map<String, Integer> monthlyConsumption = new HashMap<String, Integer>();
        namedParamJdbcTemplate.query(GET_METER_READING, paramMap,
                new ResultSetExtractor<Map<String, Integer>>() {

                    @Override
                    public Map<String, Integer> extractData(ResultSet rs)
                            throws SQLException, DataAccessException {
                        while (rs.next()) {
                            monthlyReading.put(
                                    rs.getString("month"),
                                    rs.getInt("reading"));
                            monthlyConsumption.put(
                                    rs.getString("month"),
                                    rs.getInt("consumption"));
                            meter.setProfileName(rs.getString("profile_name"));
                        }
                        meter.setMeterId(meterId);
                        meter.setMonthlyReading(monthlyReading);
                        meter.setMonthlyConsumption(monthlyConsumption);
                        return monthlyConsumption;
                    }
                });
        meter.setMeterId(meterId);
        meter.setMonthlyReading(monthlyReading);
        meter.setMonthlyConsumption(monthlyConsumption);

        return meter;
    }

}
