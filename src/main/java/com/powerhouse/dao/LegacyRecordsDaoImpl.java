package com.powerhouse.dao;

import com.powerhouse.config.ApplicationConfiguration;
import com.powerhouse.model.MeterReading;
import com.powerhouse.model.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by bilalilyas on 03/04/17.
 */

@Service
public class LegacyRecordsDaoImpl implements ILegacyRecordsDao {

    private static final Logger log = LoggerFactory.getLogger(LegacyRecordsDaoImpl.class);

    @Autowired
    ApplicationConfiguration appConfig;

    @Override
    public void insertProfileFraction() {

        String csvFile = appConfig.getLegacyFilesDir()+appConfig.getFileProfiles();
        String line = "";
        String cvsSplitBy = ",";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {

                log.info(String.format("Records %s", line));
            }
            /*
            * TODO:Prepare an array of Profiles and pass to DAO
            * */

        } catch (IOException e) {
            log.error(e.getMessage(),e);
        }
    }

    @Override
    public void insertMeterReading(MeterReading meter) {

        String csvFile = appConfig.getLegacyFilesDir()+appConfig.getFileMeterReading();
        String line = "";
        String cvsSplitBy = ",";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {

                log.info(String.format("Records %s",line));
            }
            /*
            * TODO:Prepare an array of MeterReading and pass to DAO
            * */

        } catch (IOException e) {
            log.error(e.getMessage(),e);
        }

    }
}
