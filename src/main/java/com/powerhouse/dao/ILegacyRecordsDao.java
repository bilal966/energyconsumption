package com.powerhouse.dao;

import com.powerhouse.model.MeterReading;
import org.springframework.stereotype.Service;

/**
 * Created by bilalilyas on 03/04/17.
 */

public interface ILegacyRecordsDao {

    public void insertProfileFraction();

    public void insertMeterReading(MeterReading meter);
}
