package com.powerhouse.dao;

import org.h2.jdbcx.JdbcConnectionPool;

import javax.sql.DataSource;


/**
 * Created by bilalilyas on 01/04/17.
 */
public class DBConnection {

    private static DataSource dataSource = null;

    public static synchronized DataSource getDataSource() {
        if (dataSource == null) {

            try {
                Class.forName("org.h2.Driver");
                dataSource = JdbcConnectionPool.create("jdbc:h2:mem:powerhouse;MODE=Oracle", "sa", "");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return dataSource;
    }

}
