package com.powerhouse.dao;

import com.powerhouse.model.MeterReading;
import com.powerhouse.model.Profile;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created by bilalilyas on 03/04/17.
 */


public interface IEnergyConsumptionDao {

    public void insertProfile(Profile profile);

    public Profile getProfile(String profileName);

    public void insertMeterReading(MeterReading meter);

    public MeterReading getMeterReading(int meterId);
}
