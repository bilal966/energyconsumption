package com.powerhouse.businesslogic;

import com.powerhouse.config.ApplicationConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by bilalilyas on 03/04/17.
 */
public class LegacyDataProcessor {
    private static final Logger log = LoggerFactory.getLogger(LegacyDataProcessor.class);

    @Autowired
    ApplicationConfiguration appConfig;

    public static void main(String[] args) {

        new LegacyDataProcessor().fileParser();

    }

    private void fileParser(){

        log.info(appConfig.getLegacyFilesDir());
        String csvFile = appConfig.getLegacyFilesDir()+appConfig.getFileProfiles();
        String line = "";
        String cvsSplitBy = ",";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {

                log.debug(String.format("Records %s",line));
            }

        } catch (IOException e) {
            log.error(e.getMessage(),e);
        }
    }
}
