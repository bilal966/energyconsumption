package com.powerhouse.businesslogic;

import com.powerhouse.util.ApplicationConstant;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bilalilyas on 03/04/17.
 */
public class MonthlyConsumptionProcessor {

    public Map<String,Integer> processMonthlyConsumption(Map<String,Integer> monthlyReading){
        Map<String,Integer> monthlyConsumption = new HashMap<String,Integer>();
        Map.Entry<String,Integer> prev = null;
        for(Map.Entry<String,Integer> entry : monthlyReading.entrySet()){
            if(entry.getKey() == ApplicationConstant.monthJan){
                prev = entry;
            }
            if(prev != null && entry.getValue() < prev.getValue()){
                int consumption = entry.getValue()-prev.getValue();
                monthlyConsumption.put(entry.getKey(), consumption);
            }else{
                monthlyConsumption.put(entry.getKey(), entry.getValue());
            }
            prev = entry;
        }
        return monthlyConsumption;
    }
}
