package com.powerhouse.service;

import com.powerhouse.dao.IEnergyConsumptionDao;
import com.powerhouse.model.MeterReading;
import com.powerhouse.model.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created by bilalilyas on 21/03/17.
 */

@Service
public class EnergyCousumptionService {

    @Autowired
    private IEnergyConsumptionDao energyConsumptionDao;

    public void createProfile(Profile profile){
        energyConsumptionDao.insertProfile(profile);
    }

    public Profile getProfile(String profileName){
        return energyConsumptionDao.getProfile(profileName);
    }

    public void createMeterReading(MeterReading meter){
        energyConsumptionDao.insertMeterReading(meter);
    }

    public MeterReading getMeterReading(int meterId){
        return energyConsumptionDao.getMeterReading(meterId);
    }
}
