package com.powerhouse.service;

import com.powerhouse.dao.ILegacyRecordsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


/**
 * Created by bilalilyas on 03/04/17.
 */

@Service
public class LegacyDataService {

   @Autowired
    private ILegacyRecordsDao legacyDao;

    public void insertLegacyProfiles(){
        legacyDao.insertProfileFraction();
    }
}
