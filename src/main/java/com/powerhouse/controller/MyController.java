package com.powerhouse.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.powerhouse.model.Customer;
import com.powerhouse.config.ApplicationConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

/**
 * Created by bilalilyas on 20/03/17.
 */

@Component
@Path("/myapp")
@Produces(MediaType.APPLICATION_JSON)
public class MyController {

    private static final Logger log = LoggerFactory.getLogger(MyController.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    ApplicationConfiguration appConfig;

    @GET
    @Produces("application/json")
    @Path("/health")
    public String health() {
        return new String("Jersey: Up and Running!");
    }


    @Path("/test")
    @GET
    public String test(){
        return "Application Up!";
    }


    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Path(value = "/customers")
    public Response allCustomers(){
        Response response=null;
        JSONPObject object = new JSONPObject("",null);


        ArrayList<Customer> customers = new ArrayList<>();


        jdbcTemplate.query(
                "SELECT id, first_name, last_name FROM customers WHERE first_name = ?", new Object[] { "Josh" },
                (rs, rowNum) -> new Customer(rs.getLong("id"), rs.getString("first_name"), rs.getString("last_name"))
        ).forEach(customer -> customers.add(customer));

        Customer c1 = new Customer(1,"Ali","Ahmad");
        Customer c2 = new Customer(2,"Bilal","Ahmad");
        Customer c3 = new Customer(3,"Bi","Ahmad");
        customers.add(c1);
        customers.add(c2);
        customers.add(c3);
        log.info("Total Customers size =" + customers.size());
        log.info("Application Title=" + appConfig.getTitle());


        response = Response.status(Response.Status.OK).entity(customers).build();
        return response;
    }

    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Path(value = "/customer")
    public Response getCustomer(){
        Response response=null;
        JSONPObject object = new JSONPObject("",null);
        Customer customer = new Customer(2,"Bilal","Ahmad");

        response = Response.status(Response.Status.OK).entity(customer).build();
        return response;
    }

}
