package com.powerhouse.controller;

import com.powerhouse.config.ApplicationConfiguration;
import com.powerhouse.dao.EnergyConsumptionDaoImpl;
import com.powerhouse.model.MeterReading;
import com.powerhouse.model.Profile;
import com.powerhouse.service.EnergyCousumptionService;
import com.powerhouse.service.LegacyDataService;
import com.powerhouse.util.ApplicationConstant;
import com.powerhouse.util.ApplicationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by bilalilyas on 03/04/17.
 */

@RestController
@RequestMapping(value = "/api")
public class EnergyConsumptionController {

    private static final Logger log = LoggerFactory.getLogger(EnergyConsumptionController.class);

    @Autowired
    private EnergyCousumptionService energyCousumptionService;

    @Autowired
    private LegacyDataService legacyDataService;

    @Autowired
    ApplicationConfiguration appConfig;

    @RequestMapping(value = "/")
    public ModelAndView home(ModelAndView model)
    {
        model.addObject("message", "Welcome to Powerhouse Energy Consumption Services.");
        model.setViewName("home");

        return model;
    }

    @RequestMapping(value="/profile/{profileName}",method= RequestMethod.GET)
    public Profile  getProfile(@PathVariable("profileName") String profileName) {
        return energyCousumptionService.getProfile(profileName);
    }

    @RequestMapping(value="/profile/add/{profileName}",method=RequestMethod.PUT)
    public String  createProfile(@PathVariable("profileName") String profileName) {
        String responseMsg=String.format("Profile %s added successfully",profileName);
        try{
            Profile profile = ApplicationUtil.prepareProfile(profileName);
            energyCousumptionService.createProfile(profile);
        }catch (Exception e){
            responseMsg = "Failed to add profile";
            log.error(e.getMessage(),e);
        }
        return responseMsg;
    }

    @RequestMapping(value="/meter/add/{meterId}/{profileName}",method=RequestMethod.PUT)
    public void createMeterReading(@PathVariable("meterId") int meterId,@PathVariable("profileName") String profileName){
        MeterReading meterReading = ApplicationUtil.prepareMeterReading(meterId,profileName);
        try{
            energyCousumptionService.createMeterReading(meterReading);
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }

    }

    @RequestMapping(value="/meter/{meterId}",method=RequestMethod.GET)
    public MeterReading getMeterReading(@PathVariable("meterId") int meterId){
        return energyCousumptionService.getMeterReading(meterId);
    }

    @RequestMapping(value="/add/legacy/profiles", method = RequestMethod.PUT, headers="Accept=application/json")
    public String legacyProfiles() {

        String resultMessage="Profiles processed successfully";

        try {
            legacyDataService.insertLegacyProfiles();
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            resultMessage="Failed to process Profiles.";
        }

        return resultMessage;
    }

    @RequestMapping(value="/add/legacy/meterreading", method = RequestMethod.PUT,headers="Accept=application/json")
    public String legacyMeterReading() {

        String resultMessage="Meter Reading legacy data processed successfully";


        return resultMessage;
    }

    /**
     * Method that can be used to check wether application is up or not
     *
     * @return String
     */
    @RequestMapping("/test")
    public String test() {
        return "Application Service is up and running.";
    }


}
