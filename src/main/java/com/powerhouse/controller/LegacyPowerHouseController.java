package com.powerhouse.controller;

import com.powerhouse.config.ApplicationConfiguration;
import com.powerhouse.dao.LegacyRecordsDaoImpl;
import com.powerhouse.service.LegacyDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by bilalilyas on 02/04/17.
 */

@Controller
public class LegacyPowerHouseController {

    private static final Logger log = LoggerFactory.getLogger(LegacyPowerHouseController.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    ApplicationConfiguration appConfig;

    @Autowired
    private LegacyDataService legacyDataService;


    @RequestMapping(value="/profiles", method = RequestMethod.GET,headers="Accept=application/json")
    public String legacyProfiles() {

        String resultMessage="Profiles processed successfully";

        try {
            legacyDataService.insertLegacyProfiles();
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            resultMessage="Failed to process Profiles.";
        }

        return resultMessage;
    }

    @RequestMapping(value="/meterreading", method = RequestMethod.GET,headers="Accept=application/json")
    public String legacyMeterReading() {

        String resultMessage="Meter Reading legacy data processed successfully";
        String csvFile = appConfig.getLegacyFilesDir()+appConfig.getFileMeterReading();
        String line = "";
        String cvsSplitBy = ",";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {

                System.out.println(String.format("Records %s",line));
            }

        } catch (IOException e) {
            e.printStackTrace();
            resultMessage="Failed to process Meter Reading legacy data.";
        }

        return resultMessage;
    }

}
