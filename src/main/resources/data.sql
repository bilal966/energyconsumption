INSERT INTO customer VALUES(1, 'Putka Oy', 'Vankilakatu 1', '00100', 'Helsinki', 'Finland');
INSERT INTO customer VALUES(2, 'Putka Oy', 'Vankilakatu 1', '00100', 'Lahore', 'Pakistan');
INSERT INTO customer VALUES(3, 'Bilal', 'Garden Town', '4400', 'Lahore', 'Pakistan');

INSERT INTO orders VALUES(1, 1);
INSERT INTO orders VALUES(2, 1);
INSERT INTO orders VALUES(3, 2);
INSERT INTO orders VALUES(4, 3);

INSERT INTO order_line VALUES(1, 1, 1, 'Nokia Lumia 1020', 1);
INSERT INTO order_line VALUES(2, 2, 3, 'Nokia Lumia 1520', 1);
INSERT INTO order_line VALUES(3, 3, 2, 'Nokia Lumia 1520', 2);
INSERT INTO order_line VALUES(4, 4, 1, 'Nokia Lumia 1020', 2);